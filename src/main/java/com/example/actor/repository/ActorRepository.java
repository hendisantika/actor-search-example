package com.example.actor.repository;

import com.example.actor.entity.Actor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ActorRepository extends JpaRepository<Actor, Integer> {

    @Query("select a from Actor a where a.name like %:keyword% order by a.id asc")
    List<Actor> findActors(@Param("keyword") String keyword);

}
