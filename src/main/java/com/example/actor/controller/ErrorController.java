package com.example.actor.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.web.server.ErrorPage;
import org.springframework.boot.web.server.WebServerFactoryCustomizer;
import org.springframework.boot.web.servlet.server.ConfigurableServletWebServerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

//import org.springframework.boot.context.embedded.ConfigurableEmbeddedServletContainer;
//import org.springframework.boot.context.embedded.EmbeddedServletContainerCustomizer;

@Controller
public class ErrorController {
  final static Logger logger = LoggerFactory.getLogger(ErrorController.class);

//  @Bean
//  public WebServerFactoryCustomizer containerCustomizer() {
//    return new WebServerFactoryCustomizer() {
//      @Override
//      public void customize(TomcatServletWebServerFactory factory) {
//        ErrorPage error404 = new ErrorPage(HttpStatus.NOT_FOUND, "/error/404");
//        ErrorPage error500 = new ErrorPage(HttpStatus.INTERNAL_SERVER_ERROR, "/error/500");
//        container.addErrorPages(error404, error500);
//        factory.
//      }
//    };
//  }

  @Bean
  public WebServerFactoryCustomizer<ConfigurableServletWebServerFactory> webServerFactoryCustomizer() {
    return (container) -> {
      ErrorPage error401Page = new ErrorPage(HttpStatus.UNAUTHORIZED, "/error/401.html");
      ErrorPage error404Page = new ErrorPage(HttpStatus.NOT_FOUND, "/error/404.html");
      ErrorPage error500Page = new ErrorPage(HttpStatus.INTERNAL_SERVER_ERROR, "/error/500.html");
      container.addErrorPages(error401Page, error404Page, error500Page);
    };
  }

  @GetMapping("/error/404")
  public String error404() {
    return "Error/404";
  }

  @GetMapping("/error/500")
  public String error500() {
    return "Error/500";
  }

}
