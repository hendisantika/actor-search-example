package com.example.actor.controller;

import com.example.actor.form.SimpleForm;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

@Controller
@RequestMapping(value = "/simple")
public class SimpleController {
    final static Logger logger = LoggerFactory.getLogger(SimpleController.class);

    /**
     * Item used to display select
     */
    final static Map<String, String> SELECT_ITEMS =
            Collections.unmodifiableMap(new LinkedHashMap<String, String>() {
                {
                    put("select_A", "A");
                    put("select_B", "B");
                    put("select_C", "C");
                    put("select_D", "D");
                    put("select_E", "E");
                }
            });
    /**
     * Item used to display check box
     */
    final static Map<String, String> CHECK_ITEMS =
            Collections.unmodifiableMap(new LinkedHashMap<String, String>() {
                {
                    put("checkbox_A", "A");
                    put("checkbox_B", "B");
                    put("checkbox_C", "C");
                    put("checkbox_D", "D");
                    put("checkbox_E", "E");
                }
            });
    /**
     * Item used to display radio button
     */
    final static Map<String, String> RADIO_ITEMS =
            Collections.unmodifiableMap(new LinkedHashMap<String, String>() {
                {
                    put("radio_A", "A");
                    put("radio_B", "B");
                    put("radio_C", "C");
                    put("radio_D", "D");
                    put("radio_E", "E");
                }
            });

    @GetMapping("favicon.ico")
    @ResponseBody
    public void returnNoFavicon() {
    }

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(String.class, new StringTrimmerEditor(true));
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
        sdf.setLenient(false);
        binder.registerCustomEditor(Date.class, new CustomDateEditor(sdf, true));
    }

    @GetMapping
    public String index(SimpleForm form, Model model) {
        model.addAttribute("selectItems", SELECT_ITEMS);
        model.addAttribute("checkItems", CHECK_ITEMS);
        model.addAttribute("radioItems", RADIO_ITEMS);
        return "Simple/index";
    }

    @PostMapping(value = "/confirm")
    public String confirm(@Validated @ModelAttribute SimpleForm form, BindingResult result, Model model) {
        if (result.hasErrors()) {
            model.addAttribute("validationError", "An invalid value was entered.");
            return index(form, model);
        }
        if (StringUtils.isNotEmpty(form.getFarea())) {
            form.setFarea(form.getFarea().replaceAll("\n", "<br/>"));
        }
        return "Simple/confirm";
    }

}
