CREATE TABLE IF NOT EXISTS actor
(
    id            int          NOT NULL AUTO_INCREMENT,
    name          varchar(100) NOT NULL,
    height        smallint,
    blood         varchar(2),
    birthday      date,
    birthplace_id smallint,
    update_at     timestamp(6) NOT NULL DEFAULT current_timestamp(6) ON UPDATE CURRENT_TIMESTAMP (6),
    PRIMARY KEY (id)
) engine = INNODB;