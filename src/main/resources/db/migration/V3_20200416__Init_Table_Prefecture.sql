create table if not exists prefecture(
                                         id   smallint     NOT NULL,
                                         name varchar(100) NOT NULL,
                                         PRIMARY KEY (id)
) engine = INNODB;