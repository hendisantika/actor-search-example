insert into prefecture (id, name) values
(1, 'Jakarta'),
(2, 'Bandung'),
(3, 'Depok'),
(4, 'Bekasi'),
(5, 'Cimahi'),
(6, 'Surabaya'),
(7, 'Makassar'),
(8, 'Manado'),
(9, 'Padang'),
(10, 'Palembang');

insert into actor (name, height, blood, birthday, birthplace_id) values
('Hendi Santika', 175, 'O',  '1922-07-17', 5),
('Uzumaki Naruto', 175, 'O',  '1949-12-16', 3),
('Uchiha Sasuke',   173, null, '1938-02-04', 2),
('Hatake Kakashi', 171, 'O',  '1953-05-17',  4),
('Haruno Sakura', null, null,'1947-05-10',  10),
('Sarutobi Sandaime', null, null, '1909-02-12', 1),
('Hashirama Senju',   173, 'B',   '1937-07-20', 10),
('Tobirama Senju', 167, 'A',   '1947-05-21', 3),
('Tsunade',   null, null, '1913-01-12', 3),
('Orochimaru', 155, 'B',   '1926-02-28', 1),
('Jiraiya',   null, null, '1904-05-13', 4),
('Namikaze Minato', null, null, '1915-10-17', 8),
('Asuma Sarutobi',   173,  'B',  '1928-03-10', 9);


