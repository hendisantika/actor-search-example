# Overview

It is a web application that makes simple search using MySQL and Spring Boot.
This web application handles information of "actors", lists, registers and deletes data.

**Development environment**

Development and operation confirmation was done in the following environment.

* MacOS Mojave 10.14 (64bit)
* Java 1.8.0_121
* Spring Boot 1.5.16.RELEASE version
* thymeleaf 3.0.9
* logback 1.1.3
* MySQL CE 5.8
* IntelliJ IDEA 2018.2
* Maven 3.5.4

**Advance preparation**

Prepare the sample data as follows.

* Database: `sample_db`
* User : `root`
* Table: `actor`, `prefecture`

Sql, which creates databases, users, tables, sample data, is in resources / data.

## table definition

The definition of the table to be used is as follows.

**actor**

It is a table with information of actors.

|field               |data Type  |description              |
|:-------------------|:----------|:------------------------|
|id                  |integer    |ID                       |
|name                |String     |name                     |
|height              |integer    |height                   |
|blood               |String     |blood type               |
|birthday            |Date       |birthday                 |
|birthplace_id       |integer    |Birthplace ID            |
|update_at           |Date       |Data update date         |

**prefecture**

It is a prefecture master.

|field               |data Type  |description              |
|:-------------------|:----------|:------------------------|
|id                  |integer    |ID                       |
|name                |String     |name                     |

## Execution

With the MySQL server running, execute the application with the following mvn command.

```bash
> mvn clean spring-boot:run
```

http://localhost:8080/actor

Actuator

Actuator

Actuator can check the status of spring boot from the browser. 
To enable the feature `endpoints.enabledset` application.yml to true.

| ID            | endpoint                          |
| ------------- | --------------------------------- |
| autoconfig    | http://localhost:8080/autoconfig  |
| beans	        | http://localhost:8080/beans       |
| configprops   | http://localhost:8080/configprops |
| dump	        | http://localhost:8080/dump        |
| env	        | http://localhost:8080/env         |
| health	    | http://localhost:8080/health      |
| metrics       | http://localhost:8080/metrics     |
| mappings      | http://localhost:8080/mappings    |
| trace	        | http://localhost:8080/trace       |
